package testhelpers

import (
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type seeder struct {
	db    *gorm.DB
	seeds map[string]RevertSeed
}

type RevertSeed func() error

type Seeder interface {
	Save(name string, table string, dst interface{}) RevertSeed
	Flush() error
}

func NewSeeder(db *gorm.DB) Seeder {
	return &seeder{
		db:    db,
		seeds: make(map[string]RevertSeed),
	}
}

func (s *seeder) Save(name string, table string, dst interface{}) RevertSeed {
	result := s.db.Table(table).Save(dst)
	if err := result.Error; err != nil {
		logrus.WithError(result.Error).WithField("seed_name", name).Error("cannot run seed")
	}

	s.seeds[name] = func() error {
		if err := result.Error; err != nil {
			return err
		}

		delete(s.seeds, name)
		return s.db.Table(table).Unscoped().Delete(dst).Error
	}

	return s.seeds[name]
}

func (s *seeder) Flush() error {
	for _, revert := range s.seeds {
		if err := revert(); err != nil {
			return err
		}
	}
	return nil
}
