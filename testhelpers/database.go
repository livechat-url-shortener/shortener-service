package testhelpers

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func MakeDatabaseConnection() *gorm.DB {
	cfg := LoadTestConfig()
	log.WithField("dsn", cfg.GetDSN()).Debug("database used to e2e tests")

	db, err := gorm.Open(mysql.Open(cfg.GetDSN()), &gorm.Config{})
	if err != nil {
		log.WithError(err).Panic("cannot connect to the database")
	}

	return db
}
