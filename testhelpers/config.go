package testhelpers

import (
	"fmt"

	"github.com/go-playground/validator"
	"github.com/sherifabdlnaby/configuro"
	log "github.com/sirupsen/logrus"
)

type TestConfig struct {
	Database struct {
		Host string `validate:"required"`
		Port int    `validate:"required"`
		User string `validate:"required"`
		Pass string `validate:"required"`
		Name string `validate:"required"`
	}
}

type databaseConfig struct {
	Host string
	Port int
	User string
	Pass string
	Name string
}

func LoadTestConfig() *TestConfig {
	configOptions := []configuro.ConfigOptions{
		configuro.WithLoadFromEnvVars("CONFIG"),
		configuro.WithLoadFromConfigFile("../config-test.yml", false),
		configuro.WithLoadFromConfigFile("../config.yml", false),
		configuro.WithValidateByTags(),
		configuro.WithValidateByFunc(false, true),
		configuro.Tag("config", "validate"),
	}

	configLoader, err := configuro.NewConfig(configOptions...)
	if err != nil {
		log.WithError(err).Fatal("Cannot create configuration loader")
	}

	config := &TestConfig{}
	if err := configLoader.Load(&config); err != nil {
		log.WithError(err).Fatal("Cannot load configuration")
	}

	if err := configLoader.Validate(config); err != nil {
		switch typedErr := err.(type) {
		case validator.ValidationErrors:
			logValidationErrors(typedErr)
		case configuro.ErrValidationErrors:
			logConfiguroErrors(typedErr)
		default:
			log.WithError(err).Fatal("To start an application you need to have valid configuration!")
		}
	}

	return config
}

func (c *TestConfig) GetDSN() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?parseTime=true",
		c.Database.User,
		c.Database.Pass,
		c.Database.Host,
		c.Database.Port,
		c.Database.Name,
	)
}

func logValidationErrors(err validator.ValidationErrors) {
	log.Fatal("To start an application you need to have valid configuration!")
}

func logConfiguroErrors(err configuro.ErrValidationErrors) {
	for _, e := range err.Errors() {
		log.Warn(e)
	}
	log.Fatal("To start an application you need to have valid configuration!")
}
