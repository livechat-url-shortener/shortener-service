module gitlab.com/devmint/url-shortener/shortener-service

go 1.15

require (
	github.com/devMint/go-http-runner v0.0.0-20201019190137-ea3b9b7575ba
	github.com/devMint/go-restful v0.0.0-20201014202331-a425f959164f
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/pborman/uuid v1.2.1
	github.com/rs/cors v1.7.0
	github.com/sherifabdlnaby/configuro v0.0.2
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/go-playground/validator.v9 v9.31.0
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.7
)
