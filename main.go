package main

import (
	"net/http"

	"github.com/devMint/go-restful"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-playground/validator"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	cfg := LoadConfig()
	if loggerLevel, err := cfg.GetLoggerLevel(); err == nil {
		log.SetLevel(loggerLevel)
	}

	// INFRASTRUCTURE
	validator := validator.New()
	db, err := gorm.Open(mysql.Open(cfg.GetDatabaseDSN()), &gorm.Config{
		Logger: NewDatabaseLogger(logrus.New()),
	})
	if err != nil {
		log.WithError(err).Panic("Connection to DB failed")
	}

	// INTERNAL APPLICATIONS
	shortenerApp := module_shortener.NewShortenerModule(module_shortener.ShortenerModuleOptions{
		DB:                       db,
		Validator:                validator,
		AllowToRunAutoMigrations: cfg.Application.AllowAutoMigrations,
	})

	// HTTP SERVER
	chiRouter := chi.NewRouter()
	chiRouter.Use(middleware.RequestID)
	chiRouter.Use(middleware.RealIP)
	chiRouter.Use(middleware.Recoverer)
	chiRouter.Use(RouterLogger(logrus.New()))
	chiRouter.Use(cors.Default().Handler)

	router := restful.NewRouter(chiRouter, restful.RouterOptions{
		Validator: validator,
	})

	router.Mount("/v1/urls", shortenerApp.GetRouter())

	if err := http.ListenAndServe(cfg.Application.HTTP.Address, router); err != nil {
		log.WithError(err).Panic("HTTP server crashed")
	}
}
