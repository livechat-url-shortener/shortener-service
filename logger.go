package main

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type gormLogger struct {
	log                   *logrus.Logger
	SlowThreshold         time.Duration
	SkipErrRecordNotFound bool
}

func NewDatabaseLogger(log *logrus.Logger) logger.Interface {
	return &gormLogger{log: log}
}

func (l *gormLogger) LogMode(level logger.LogLevel) logger.Interface { return l }
func (l *gormLogger) Info(ctx context.Context, msg string, args ...interface{}) {
	l.log.WithContext(ctx).Infof(msg, args...)
}
func (l *gormLogger) Warn(ctx context.Context, msg string, args ...interface{}) {
	l.log.WithContext(ctx).Warnf(msg, args...)
}
func (l *gormLogger) Error(ctx context.Context, msg string, args ...interface{}) {
	l.log.WithContext(ctx).Errorf(msg, args...)
}
func (l *gormLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	elapsed := time.Since(begin)
	sql, _ := fc()

	entry := l.log.WithContext(ctx)
	entry = entry.WithField("elapsed", elapsed)

	if err != nil && !(errors.Is(err, gorm.ErrRecordNotFound) && l.SkipErrRecordNotFound) {
		entry.WithError(err).Error(sql)
		return
	}

	entry.Debug(sql)
}

func RouterLogger(logger *logrus.Logger) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			reqID := middleware.GetReqID(r.Context())
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			t1 := time.Now()
			defer func() {
				remoteIP, _, err := net.SplitHostPort(r.RemoteAddr)
				if err != nil {
					remoteIP = r.RemoteAddr
				}
				scheme := "http"
				if r.TLS != nil {
					scheme = "https"
				}
				fields := logrus.Fields{
					"status_code":      ww.Status(),
					"bytes":            ww.BytesWritten(),
					"duration":         int64(time.Since(t1)),
					"duration_display": time.Since(t1).String(),
					"remote_ip":        remoteIP,
					"proto":            r.Proto,
					"method":           r.Method,
				}
				if len(reqID) > 0 {
					fields["request_id"] = reqID
				}
				logger.WithFields(fields).Infof("%s://%s%s", scheme, r.Host, r.RequestURI)
			}()

			h.ServeHTTP(ww, r)
		}

		return http.HandlerFunc(fn)
	}
}
