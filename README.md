# Shortener Service

**Shortener Service** allows to hide `string` values under `UUIDv4` key. Main purpose is to 
create secure *URL shortener* where each UUID stores one URL. 

## Application

### Technology

Application is written with **Golang 1.15.5**. Service requires also MariaDB and is tested 
with **MariaDB 10.5.6**.

### Infrastructure

![](./docs/infrastructure.png)

### Endpoints

Documentation for endpoints is stored in `./docs/swagger.yml` file in Swagger format.

## Development

### Installation

* Copy the repository
* Copy the dependencies:

```bash
$ go mod vendor
```

### Configuration

Application can be configured in differewany ways:

* using environment variables starting with `CONFIG` prefix
* using environment variables stored in `.env` file
* using `config.yml` file (file `config.yml.dist` stores a representational config which can be used with `docker-compose.yml` configuration)

Example of environment variables or `.env` file:

```env
CONFIG_DATABASE_NAME=shortener_db
CONFIG_DATABASE_USER=root
CONFIG_DATABASE_PASS=root
CONFIG_DATABASE_HOST=localhost
CONFIG_DATABASE_PORT=3306
CONFIG_LOGGER_LEVEL=debug
CONFIG_APPLICATION_ALLOWAUTOMIGRATIONS=true
CONFIG_APPLICATION_HTTP_ADDRESS=:8080
```

### Running

To run application just execute command:

```bash
$ go run .
```

To run binary on UNIX systems:

```bash
$ make build_binary
$ ./build_binary
```

### Tests

To run tests just execute command:

```bash
$ make run_tests
```

Tests requires database to work and uses the same configuration as the app. You change this 
behaviour by creating the file `config-test.yml` in the main directory with the same structure
what `config.yml` but with different values.