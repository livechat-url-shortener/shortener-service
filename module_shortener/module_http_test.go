package module_shortener

import (
	"net/http"
	"net/http/httptest"
	"testing"

	httprunner "github.com/devMint/go-http-runner"
	"github.com/devMint/go-restful"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
	"gitlab.com/devmint/url-shortener/shortener-service/testhelpers"
	"gorm.io/gorm"
)

type seedEntity struct {
	Name  string
	Table string
}

var shortenedEntitiesTable = "shortened_entities"
var seedsShortenedEntities = map[string]domain.ShortenedEntity{
	"t01": {Hash: "316ebf92-282e-4b06-bce5-b5227aafd1d0", Value: "https://www.uuidgenerator.net/version4"},
}

func Test_ModuleShortener_Find(t *testing.T) {
	validator := validator.New()
	db := testhelpers.MakeDatabaseConnection()
	seeder := testhelpers.NewSeeder(db)

	runSeeds := func(r *http.Request) {
		for name, entity := range seedsShortenedEntities {
			seeder.Save(name, shortenedEntitiesTable, &entity)
		}
	}

	runner := httprunner.EndToEndTestRunner{
		Server: makeTestServer(db, validator),
		After: func(r *httptest.ResponseRecorder) {
			seeder.Flush()
		},
	}

	runner.GET(t, "/v1/316ebf92-282e-4b06-bce5-b5227aafd1d0", httprunner.Expectations{
		Name:       "JSON shortened entity exists",
		Before:     runSeeds,
		StatusCode: http.StatusMovedPermanently,
		Headers: map[string]string{
			"Location": "https://www.uuidgenerator.net/version4",
		},
	})

	runner.GET(t, "/v1/316ebf92-282e-4b06-bce5-b5227aafd1d0", httprunner.Expectations{
		Name:       "XML shortened entity exists",
		StatusCode: http.StatusMovedPermanently,
		Before: func(r *http.Request) {
			r.Header.Set("Content-type", "application/xml")
			runSeeds(r)
		},
		Headers: map[string]string{
			"Location": "https://www.uuidgenerator.net/version4",
		},
	})

	runner.GET(t, "/v1/316ebf92-282e-4b06-bce5-b5227aafd1d0", httprunner.Expectations{
		Name:       "shortened entity does not exist",
		StatusCode: http.StatusNotFound,
		Body: `{
			"type":"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			"title":"Not Found",
			"detail":"Entity not found",
			"status":404
		}`,
	})

	runner.GET(t, "/v1/lorem-ipsum", httprunner.Expectations{
		Name:       "url pass only uuid values",
		StatusCode: http.StatusBadRequest,
		Body: `{
			"type":"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			"title":"Bad Request",
			"detail":"Validation error",
			"status":400
		}`,
	})

	runner.POST(t, "/v1", `{"url": "https://pkg.go.dev/"}`, httprunner.Expectations{
		Name:       "JSON create new shorten url",
		StatusCode: http.StatusOK,
		BodyContains: []string{
			`"https://pkg.go.dev/"`,
		},
	})

	requestBodyXML := `<request>
		<url>https://pkg.go.dev/</url>
	</request>`
	runner.POST(t, "/v1", requestBodyXML, httprunner.Expectations{
		Name:       "XML create new shorten url",
		StatusCode: http.StatusOK,
		Headers: map[string]string{
			"Content-type": "application/xml",
		},
		BodyContains: []string{
			"<url>https://pkg.go.dev/</url>",
		},
		Before: func(r *http.Request) {
			r.Header.Set("Content-type", "application/xml")
		},
	})

	runner.POST(t, "/v1", `{"url": "this-is-not-url"}`, httprunner.Expectations{
		Name:       "validation error when url is not url",
		StatusCode: http.StatusBadRequest,
		Body: `{
			"type":"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			"title":"Bad Request",
			"detail":"Validation error",
			"status":400
		}`,
	})

	runner.POST(t, "/v1", `{}`, httprunner.Expectations{
		Name:       "validation error when body is empty",
		StatusCode: http.StatusBadRequest,
		Body: `{
			"type":"http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html",
			"title":"Bad Request",
			"detail":"Validation error",
			"status":400
		}`,
	})
}

func makeTestServer(db *gorm.DB, validator *validator.Validate) http.Handler {
	app := NewShortenerModule(ShortenerModuleOptions{
		DB:                       db,
		Validator:                validator,
		AllowToRunAutoMigrations: true,
	})

	router := restful.NewRouter(chi.NewRouter())
	router.Mount("/v1", app.GetRouter())

	return router
}
