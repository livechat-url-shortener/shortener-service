package infrastructure

import (
	"errors"
	"strings"

	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
	"gorm.io/gorm"
)

func mapInfrastructureErrorToDomain(err error) error {
	if err == nil {
		return nil
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return domain.EntityNotFound(err)
	}
	if strings.Contains(err.Error(), "Error 1062: Duplicate entry") {
		return domain.DuplicateEntry(err)
	}

	return domain.InfrastructureError(err)
}
