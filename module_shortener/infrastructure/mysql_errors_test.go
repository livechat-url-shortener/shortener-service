package infrastructure

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
	"gorm.io/gorm"
)

func Test_MapInfrastructureErrorToDomain(t *testing.T) {
	t.Run("map gorm.ErrRecordNotFound to domain.EntityNotFound", func(t *testing.T) {
		err := mapInfrastructureErrorToDomain(gorm.ErrRecordNotFound)
		isOk := errors.Is(domain.EntityNotFound(gorm.ErrRecordNotFound), err)
		assert.True(t, isOk)
	})

	t.Run("map gorm.ErrUnsupportedDriver to domain.InfrastructureError", func(t *testing.T) {
		err := mapInfrastructureErrorToDomain(gorm.ErrUnsupportedDriver)
		isOk := errors.Is(domain.InfrastructureError(gorm.ErrUnsupportedDriver), err)

		assert.True(t, isOk)
	})

	t.Run("map duplicated entry string to domain.DuplicateEntry", func(t *testing.T) {
		rawError := errors.New("Error 1062: Duplicate entry '316ebf92-282e-4b06-bce5-b5227aafd1d0' for key 'hash'")
		err := mapInfrastructureErrorToDomain(rawError)
		isOk := errors.Is(domain.DuplicateEntry(rawError), err)

		assert.True(t, isOk)
	})
}
