package infrastructure

import (
	"context"
	"fmt"

	"github.com/go-playground/validator"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
)

func NewValidator(v *validator.Validate) domain.Validator {
	return &domainValidator{v: v}
}

type domainValidator struct {
	v *validator.Validate
}

func (v *domainValidator) Validate(ctx context.Context, obj interface{}) error {
	err := v.v.StructCtx(ctx, obj)
	if err != nil {
		switch validErr := err.(type) {
		case validator.ValidationErrors:
			return mapValidatorErrorToDomainError(validErr)
		default:
			return domain.ValidationError(validErr)
		}
	}

	return nil
}

func mapValidatorErrorToDomainError(err validator.ValidationErrors) error {
	translatedErr := fmt.Errorf("Key: '%s' Error:Field validation for '%s' failed on the '%s' tag", err[0].Namespace(), err[0].Field(), err[0].Tag())
	return domain.ValidationError(translatedErr)
}
