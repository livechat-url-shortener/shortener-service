package infrastructure

import (
	"context"
	"errors"

	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
	"gorm.io/gorm"
)

type ShortenerRepository interface {
	domain.InternalShortenerRepository
}

type mysqlShortenerRepository struct {
	db *gorm.DB
}

func NewMySQLShortenerRepository(db *gorm.DB) ShortenerRepository {
	return &mysqlShortenerRepository{db: db}
}

func (r *mysqlShortenerRepository) FindBy(ctx context.Context, opts domain.FindByOptions) (domain.ShortenedEntity, error) {
	var (
		entity domain.ShortenedEntity
		err    error
	)

	switch {
	case opts.ID > 0:
		err = r.db.WithContext(ctx).First(&entity, opts.ID).Error
	case opts.Hash != "":
		err = r.db.WithContext(ctx).First(&entity, "hash = ?", opts.Hash).Error
	default:
		err = domain.InfrastructureError(errors.New("invalid option 'domain.FindByOptions'"))
	}

	if err != nil {
		return entity, mapInfrastructureErrorToDomain(err)
	}

	return entity, err
}

func (r *mysqlShortenerRepository) Save(ctx context.Context, msg domain.SaveShortenedEntityOptions) error {
	return mapInfrastructureErrorToDomain(r.db.WithContext(ctx).Save(&msg.Entity).Error)
}
