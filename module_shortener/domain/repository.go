package domain

import (
	"context"
)

type shortenerRepository struct {
	repository ShortenerRepository
	validator  Validator
}

func NewShortenerRepository(repository InternalShortenerRepository, validator Validator) ShortenerRepository {
	return &shortenerRepository{
		repository: repository,
		validator:  validator,
	}
}

func (r *shortenerRepository) FindBy(ctx context.Context, opts FindByOptions) (ShortenedEntity, error) {
	if err := r.validator.Validate(ctx, opts); err != nil {
		return ShortenedEntity{}, err
	}

	return r.repository.FindBy(ctx, opts)
}
