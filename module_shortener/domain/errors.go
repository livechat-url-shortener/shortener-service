package domain

var (
	InfrastructureError = newDomainError("Infrastructure error")
	ValidationError     = newDomainError("Validation error")
	EntityNotFound      = newDomainError("Entity not found")
	DuplicateEntry      = newDomainError("Duplicate entry")
)

type DomainError struct {
	Message string
	Details string
}

func (e DomainError) Error() string {
	return e.Message
}

func (e DomainError) Is(target error) bool {
	return e.Message == target.Error()
}

func newDomainError(msg string) func(err ...error) error {
	return func(err ...error) error {
		domainError := DomainError{Message: msg}
		if len(err) > 0 {
			domainError.Details = err[0].Error()
		}

		return domainError
	}
}
