package domain

import (
	"context"
	"database/sql"
	"time"
)

type ShortenedEntity struct {
	ID        uint   `gorm:"primaryKey"`
	Hash      string `gorm:"size:36;unique;<-:create"`
	Value     string `gorm:"size:4096;<-:create"`
	CreatedAt time.Time
	DeletedAt sql.NullTime `gorm:"index"`
}

type ShortenerService interface {
	RegisterURL(context.Context, RegisterlUrlMessage) (ShortenedEntity, error)
}

type ShortenerRepository interface {
	FindBy(context.Context, FindByOptions) (ShortenedEntity, error)
}

type InternalShortenerRepository interface {
	ShortenerRepository
	Save(context.Context, SaveShortenedEntityOptions) error
}

type Validator interface {
	Validate(context.Context, interface{}) error
}

type RegisterlUrlMessage struct {
	Value string
}

type FindByOptions struct {
	ID   int    `validate:"required_without_all=Hash"`
	Hash string `validate:"required_without_all=ID,uuid4"`
}

type SaveShortenedEntityOptions struct {
	Entity ShortenedEntity
}
