package domain

import (
	"context"

	"github.com/pborman/uuid"
)

type shortenerService struct {
	repository InternalShortenerRepository
	validator  Validator
}

func NewShortenerService(repository InternalShortenerRepository, validator Validator) ShortenerService {
	return &shortenerService{
		repository: repository,
		validator:  validator,
	}
}

func (s *shortenerService) RegisterURL(ctx context.Context, msg RegisterlUrlMessage) (ShortenedEntity, error) {
	if err := s.validator.Validate(ctx, msg); err != nil {
		return ShortenedEntity{}, ValidationError(err)
	}

	entity := ShortenedEntity{Hash: uuid.New(), Value: msg.Value}
	if err := s.repository.Save(ctx, SaveShortenedEntityOptions{Entity: entity}); err != nil {
		return entity, err
	}

	return s.repository.FindBy(ctx, FindByOptions{Hash: entity.Hash})
}
