package application

import (
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
)

func Test_MapDomainErrorToResponse(t *testing.T) {
	t.Run("map domain.EntityNotFound to response 404", func(t *testing.T) {
		response := mapDomainErrorToResponse(domain.EntityNotFound())
		assert.Equal(t, http.StatusNotFound, response.StatusCode())
	})

	t.Run("map domain.ValidationError to response 400", func(t *testing.T) {
		response := mapDomainErrorToResponse(domain.ValidationError())
		assert.Equal(t, http.StatusBadRequest, response.StatusCode())
	})

	t.Run("map domain.DuplicateEntry to response 409", func(t *testing.T) {
		response := mapDomainErrorToResponse(domain.DuplicateEntry())
		assert.Equal(t, http.StatusConflict, response.StatusCode())
	})

	t.Run("map domain.InfrastructureError to response 500", func(t *testing.T) {
		response := mapDomainErrorToResponse(domain.InfrastructureError())
		assert.Equal(t, http.StatusInternalServerError, response.StatusCode())
	})

	t.Run("map unknown error to response 500", func(t *testing.T) {
		response := mapDomainErrorToResponse(errors.New("unknown error"))
		assert.Equal(t, http.StatusInternalServerError, response.StatusCode())
	})
}
