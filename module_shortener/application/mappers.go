package application

import (
	"errors"
	"fmt"

	"github.com/devMint/go-restful/response"
	"github.com/go-playground/validator"
	"github.com/sirupsen/logrus"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
)

type findOneEntity struct {
	UUID string `json:"uuid" xml:"uuid"`
	URL  string `json:"url" xml:"url"`
}

type createBody struct {
	URL string `json:"url" xml:"url" validate:"required,url"`
}

func mapDomainErrorToResponse(err error) response.Response {
	logEntry := logrus.WithError(err)

	if domainErr, isOk := err.(domain.DomainError); isOk {
		logEntry = logEntry.WithField("details", domainErr.Details)
		logrus.WithError(domainErr).WithField("details", domainErr.Details).Info("Cannot process request due to domain error")
	}

	switch true {
	case errors.Is(err, domain.EntityNotFound(err)):
		logEntry.Info("Cannot process request due to: domain.EntityNotFound")
		return response.NotFound(err)
	case errors.Is(err, domain.ValidationError(err)):
		logEntry.Info("Cannot process request due to: domain.ValidationError")
		return response.BadRequest(err)
	case errors.Is(err, domain.DuplicateEntry(err)):
		logEntry.Warn("Cannot process request due to: domain.DuplicateEntry")
		return response.Conflict(err)
	case errors.Is(err, domain.InfrastructureError(err)):
		logEntry.Error("Cannot process request due to: domain.InfrastructureError")
		return response.InternalServerError(err)
	default:
		logEntry.Error("Cannot process request due to unknown error")
		return response.InternalServerError(err)
	}
}

func mapDomainEntityToApplication(d domain.ShortenedEntity) findOneEntity {
	return findOneEntity{
		URL:  d.Value,
		UUID: d.Hash,
	}
}

func mapValidationErrorToResponse(err error) response.Response {
	validationErr, isOk := err.(validator.ValidationErrors)
	if !isOk {
		return response.InternalServerError(err)
	}

	translatedErr := fmt.Errorf("Key: '%s' Error:Field validation for '%s' failed on the '%s' tag", validationErr[0].Namespace(), validationErr[0].Field(), validationErr[0].Tag())
	domainErr := domain.ValidationError(translatedErr)

	return response.BadRequest(domainErr)
}
