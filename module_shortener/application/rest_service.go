package application

import (
	"github.com/devMint/go-restful"
	"github.com/devMint/go-restful/request"
	"github.com/devMint/go-restful/response"
	"github.com/go-chi/chi"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
)

type ApplicationService interface {
	GetRouter() restful.Router
}

func NewShortenerApp(service domain.ShortenerService, repository domain.ShortenerRepository) ApplicationService {
	return &shortenerApp{
		domainService:    service,
		domainRepository: repository,
	}
}

type shortenerApp struct {
	domainService    domain.ShortenerService
	domainRepository domain.ShortenerRepository
}

func (a *shortenerApp) GetRouter() restful.Router {
	r := restful.NewRouter(chi.NewRouter())
	r.Get("/{hash}", a.findOne)
	r.Post("/", a.create)

	return r
}

func (a *shortenerApp) findOne(r request.Request) response.Response {
	url, err := a.domainRepository.FindBy(r.Context(), domain.FindByOptions{
		Hash: r.Param("hash"),
	})

	if err != nil {
		return mapDomainErrorToResponse(err)
	}

	return response.MovedPermanently(url.Value)
}

func (a *shortenerApp) create(r request.Request) response.Response {
	body := createBody{}
	if err := r.Body(&body); err != nil {
		return mapValidationErrorToResponse(err)
	}

	entity, err := a.domainService.RegisterURL(r.Context(), domain.RegisterlUrlMessage{Value: body.URL})
	if err != nil {
		return mapDomainErrorToResponse(err)
	}

	return response.Ok(mapDomainEntityToApplication(entity))
}
