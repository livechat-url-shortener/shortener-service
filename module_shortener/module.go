package module_shortener

import (
	"sync"

	"github.com/go-playground/validator"
	log "github.com/sirupsen/logrus"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/application"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/domain"
	"gitlab.com/devmint/url-shortener/shortener-service/module_shortener/infrastructure"
	"gorm.io/gorm"
)

var ShortenedEntity domain.ShortenedEntity
var migrations = map[string]interface{}{
	"ShortenedEntity": ShortenedEntity,
}

func NewShortenerModule(opts ShortenerModuleOptions) application.ApplicationService {
	// INFRASTRUCTURE
	mysqlRepository := infrastructure.NewMySQLShortenerRepository(opts.DB)
	validator := infrastructure.NewValidator(opts.Validator)

	// DOMAIN
	shortenerService := domain.NewShortenerService(mysqlRepository, validator)
	shortenerRepository := domain.NewShortenerRepository(mysqlRepository, validator)

	// PREPARE APPLICATION
	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		if opts.AllowToRunAutoMigrations {
			runAutoMigrations(opts.DB)
		}
	}()

	wg.Wait()

	// APPLICATION
	return application.NewShortenerApp(
		shortenerService,
		shortenerRepository,
	)
}

type ShortenerModuleOptions struct {
	DB                       *gorm.DB
	Validator                *validator.Validate
	AllowToRunAutoMigrations bool
}

func runAutoMigrations(db *gorm.DB) {
	for name, dst := range migrations {
		if err := db.AutoMigrate(dst); err != nil {
			log.WithError(err).WithField("migration_name", name).Error("cannot run auto migration")
		}
	}
}
