package main

import (
	"fmt"

	"github.com/sherifabdlnaby/configuro"
	log "github.com/sirupsen/logrus"
	"gopkg.in/go-playground/validator.v9"
)

type Config struct {
	Database struct {
		Host string `validate:"required"`
		Port int    `validate:"required"`
		User string `validate:"required"`
		Pass string `validate:"required"`
		Name string `validate:"required"`
	}
	Logger struct {
		Level string `validate:"oneof=panic fatal error warn warning info debug trace"`
	}
	Application struct {
		AllowAutoMigrations bool
		HTTP                struct {
			Address string `validate:"required"`
		}
	}
}

func LoadConfig() *Config {
	configLoaderOptions := configuro.DefaultOptions()
	configLoaderOptions = append(configLoaderOptions, configuro.WithLoadDotEnv("./.env"))

	configLoader, err := configuro.NewConfig(configLoaderOptions...)
	if err != nil {
		log.WithError(err).Fatal("Cannot create configuration loader")
	}

	config := &Config{}
	if err := configLoader.Load(&config); err != nil {
		log.WithError(err).Fatal("Cannot load configuration")
	}

	if err := configLoader.Validate(config); err != nil {
		switch typedErr := err.(type) {
		case validator.ValidationErrors:
			logValidationErrors(typedErr)
		case configuro.ErrValidationErrors:
			logConfiguroErrors(typedErr)
		default:
			log.WithError(err).Fatal("To start an application you need to have valid configuration!")
		}
	}

	return config
}

func (c *Config) GetLoggerLevel() (log.Level, error) {
	if c.Logger.Level == "" {
		return log.WarnLevel, nil
	}

	return log.ParseLevel(c.Logger.Level)
}

func (c *Config) GetDatabaseDSN() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?parseTime=true",
		c.Database.User,
		c.Database.Pass,
		c.Database.Host,
		c.Database.Port,
		c.Database.Name,
	)
}

func logValidationErrors(err validator.ValidationErrors) {
	log.Fatal("To start an application you need to have valid configuration!")
}

func logConfiguroErrors(err configuro.ErrValidationErrors) {
	for _, e := range err.Errors() {
		log.Warn(e)
	}
	log.Fatal("To start an application you need to have valid configuration!")
}
